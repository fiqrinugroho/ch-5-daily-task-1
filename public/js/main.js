// Add Data Product in dashboard admin
async function addDataProduct() {
    const data = {
      name: document.getElementById("name").value,
      size: document.getElementById("size").value,
      desc: document.getElementById("desc").value,
      brand: document.getElementById("brand").value,
      userId: document.getElementById("userId").value,
    };
    if ( data.name == "" ) {
      alert("Data Tidak Boleh Kosong!");
    } else {
      const restAPI = await axios.post("/api/products", data);
      if (restAPI.data.status) {
        alert("Berhasil Tambah Data");
      } else {
        alert("500 Server Error [Tidak Dapat Menambahkan data]");
      }
    }
}

// async function updateDataProduct() {
//   // const id = document.getElementById("id").value
//   console.log("apaaja")
//     const data = {
//       name: document.getElementById("name").value,
//       size: document.getElementById("size").value,
//       desc: document.getElementById("desc").value,
//       brand: document.getElementById("brand").value,
//       userId: document.getElementById("userId").value,
//     };
//     if ( data.name == "" ) {
//       alert("Data Tidak Boleh Kosong!");
//     } else {
//       const restAPI = await axios.put('/api/products/:id', data);
//       console.log(restAPI)
//       if (restAPI.data.status == 200) {
//         alert("Berhasil Update Data");
//       } else {
//         alert("500 Server Error [Tidak Dapat mengupdate data]");
//       }
//     }
// }
// // delete Data Product in dashboard admin
// async function deleteDataProduct() {
//     const data = {
//       id: document.getElementById("idData").value,
//     };
//     const restAPI = await axios.delete("/api/products/:id", data);
//     if (restAPI.data.status) {
//         alert("Berhasil dihapus Data");
//     } else {
//         alert("500 Server Error [Tidak Dapat Menghapus data]");
//     }
// }