const router = require("express").Router();

const productController = require('../controllers/productsController')
const adminProductController = require('../controllers/admin/productsController')

router.get('/api/products', productController.getProducts)
router.post('/api/products', productController.createProducts)
router.get('/api/products/:id', productController.getProductsById)
router.put('/api/products/:id', productController.updateProducts)
router.delete('/api/products/:id', productController.deleteProducts)

// rotes dashboard admin
router.get('/', adminProductController.getProduct)
router.get('/create', adminProductController.createProductPage)
router.post('/create', adminProductController.createProduct)
router.get('/delete/:id', adminProductController.deleteProduct)
router.get('/update/:id', adminProductController.updateProductPage)
router.post('/update/:id', adminProductController.updateProduct)

// halaman 404
router.use('/', adminProductController.pageNotFound)




module.exports = router