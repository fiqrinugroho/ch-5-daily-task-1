// import connection
const client = require("../../config/connnection") 

// GET 
async function getProduct(req, res) {
    const userId = req.query.userId
    if(userId == null){
        // raw query untuk menampilkan semua data
        const query = await client.query('SELECT * FROM products')
            // menampilkan data pada dasboard
        const productData = query.rows
        res.render("index", {
                    productData,
            });
    }else {
        const query = await client.query(`SELECT * FROM products WHERE user_id='${userId}'`)
            // menampilkan data pada dasboard
        const productData = query.rows
        res.render("index", {
                    productData,
            });
    }

};

async function createProductPage(req, res) {
    res.render("create");
};

async function createProduct(req, res) {
    const {name, size, desc, brand, userId} = req.body
    // query untuk update data peroduct, menggunakan backtip
    await client.query(`INSERT INTO products(product_name, size, description, brand, user_id) VALUES ('${name}','${size}','${desc}','${brand}','${userId}')`)
    res.redirect(200, "/");
};

async function updateProductPage(req, res) {
    const id = req.params.id
    const query = await client.query(`SELECT * FROM products WHERE id='${id}'`)
    // menampilkan data
    const productData = query.rows
    // console.log(id, productData)
    res.render("update", {
        productData
    });
};

async function updateProduct(req, res) {
    const id = req.params.id
    const {name, size, desc, brand, userId} = req.body
    // query untuk update data peroduct, menggunakan backtip
    await client.query(`UPDATE products SET product_name='${name}', size='${size}', description='${desc}', brand='${brand}', user_id='${userId}' WHERE id = '${id}'`)
    res.redirect("/");
};

async function deleteProduct(req, res) {
    const id = req.params.id
    await client.query(`DELETE FROM products WHERE id = '${id}' `)
    res.redirect("/");
};


async function pageNotFound(req, res) {
    res.render("404");
};

module.exports = {
    getProduct,
    createProduct,
    createProductPage,
    updateProduct,
    updateProductPage,
    deleteProduct,
    pageNotFound,
}