// import connection
const client = require("../config/connnection") 

// get atau retrieve function controller
async function getProducts(req, res) {
    try {
        // raw query untuk menampilkan semua data
        const query = await client.query('SELECT * FROM products')
        // menampilkan data pada
        const dataProduct = query.rows
        res.status(200).json({
            'status': 'success',
            'totalData': dataProduct.length,
            'data': dataProduct
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}
async function getProducts(req, res) {
    try {
        // raw query untuk menampilkan semua data
        const query = await client.query('SELECT * FROM products')
        // menampilkan data pada
        const dataProduct = query.rows
        res.status(200).json({
            'status': 'success',
            'totalData': dataProduct.length,
            'data': dataProduct
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}

async function getProductsById(req, res) {
    try {
        const id = req.params.id
        // raw query untuk menampilkan data dengan id tertentu, menggunkan backtip
        const query = await client.query(`SELECT * FROM products WHERE user_id='${id}'`)
        // menampilkan data pada
        const dataProduct = query.rows
        if (!dataProduct.length) {
            res.status(404).json({
                'message': `data pada id:${id} tidak ada`
            })
        }
        res.status(200).json({
            'status': 'success',
            'data': dataProduct
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}

async function createProducts(req, res) {
    try {
        const {name, size, desc, brand, userId} = req.body
        // query untuk update data peroduct, menggunakan backtip
        const query = await client.query(`INSERT INTO products(product_name, size, description, brand, user_id) VALUES ('${name}','${size}','${desc}','${brand}','${userId}')`)
        res.status(200).json({
            'status': 'success menambahkan data',
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}

async function updateProducts(req, res) {
    try {
        const id = req.params.id
        const {name, size, desc, brand, userId} = req.body
        // query untuk mengupdate data ke tabel, menggunakan backtip
        const query = await client.query (`UPDATE products SET product_name='${name}', size='${size}', description='${desc}', brand='${brand}', user_id='${userId}' WHERE id = '${id}' `)
        res.status(200).json({
            'status': 'success update data',
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}

async function deleteProducts(req, res) {
    try {
        const id = req.params.id
        // query untuk menghapus data ke tabel, menggunakan backtip
        const query = await client.query (`DELETE FROM products WHERE id = '${id}' `)
        res.status(200).json({
            'status': 'success delete data',
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}


module.exports = {
    getProducts,
    getProductsById,
    createProducts,
    updateProducts,
    deleteProducts,
}