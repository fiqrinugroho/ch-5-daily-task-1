const { Client } = require("pg")
const dotenv = require('dotenv')
dotenv.config()

const client = new Client({
    host:process.env.DB_HOST,
    user:process.env.DB_USERNAME,
    port:process.env.DB_PORT,
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME

})

client.connect(err => {
    if(err){
        console.log(err.message)
    }else {
        console.log("Database Berhasil Terkoneksi")
    }

})

module.exports = client